################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
S_SRCS += \
D:/STM32projects/VL530xFirstTry/SW4STM32/startup_stm32f030x8.s 

C_SRCS += \
D:/STM32projects/VL530xFirstTry/SW4STM32/syscalls.c 

OBJS += \
./Application/SW4STM32/startup_stm32f030x8.o \
./Application/SW4STM32/syscalls.o 

C_DEPS += \
./Application/SW4STM32/syscalls.d 


# Each subdirectory must supply rules for building sources it contributes
Application/SW4STM32/startup_stm32f030x8.o: D:/STM32projects/VL530xFirstTry/SW4STM32/startup_stm32f030x8.s
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Assembler'
	@echo $(PWD)
	arm-none-eabi-as -mcpu=cortex-m0 -mthumb -mfloat-abi=soft -g -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/SW4STM32/syscalls.o: D:/STM32projects/VL530xFirstTry/SW4STM32/syscalls.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -mfloat-abi=soft '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F030x8 -I"D:/STM32projects/VL530xFirstTry/Inc" -I"D:/STM32projects/VL530xFirstTry/Drivers/STM32F0xx_HAL_Driver/Inc" -I"D:/STM32projects/VL530xFirstTry/Drivers/STM32F0xx_HAL_Driver/Inc/Legacy" -I"D:/STM32projects/VL530xFirstTry/Drivers/CMSIS/Device/ST/STM32F0xx/Include" -I"D:/STM32projects/VL530xFirstTry/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


