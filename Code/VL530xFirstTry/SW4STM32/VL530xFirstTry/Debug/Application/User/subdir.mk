################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Application/User/VL53L0X.c \
D:/STM32projects/VL530xFirstTry/Src/gpio.c \
D:/STM32projects/VL530xFirstTry/Src/i2c.c \
D:/STM32projects/VL530xFirstTry/Src/main.c \
D:/STM32projects/VL530xFirstTry/Src/stm32f0xx_hal_msp.c \
D:/STM32projects/VL530xFirstTry/Src/stm32f0xx_it.c 

OBJS += \
./Application/User/VL53L0X.o \
./Application/User/gpio.o \
./Application/User/i2c.o \
./Application/User/main.o \
./Application/User/stm32f0xx_hal_msp.o \
./Application/User/stm32f0xx_it.o 

C_DEPS += \
./Application/User/VL53L0X.d \
./Application/User/gpio.d \
./Application/User/i2c.d \
./Application/User/main.d \
./Application/User/stm32f0xx_hal_msp.d \
./Application/User/stm32f0xx_it.d 


# Each subdirectory must supply rules for building sources it contributes
Application/User/%.o: ../Application/User/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -mfloat-abi=soft '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F030x8 -I"D:/STM32projects/VL530xFirstTry/Inc" -I"D:/STM32projects/VL530xFirstTry/Drivers/STM32F0xx_HAL_Driver/Inc" -I"D:/STM32projects/VL530xFirstTry/Drivers/STM32F0xx_HAL_Driver/Inc/Legacy" -I"D:/STM32projects/VL530xFirstTry/Drivers/CMSIS/Device/ST/STM32F0xx/Include" -I"D:/STM32projects/VL530xFirstTry/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/User/gpio.o: D:/STM32projects/VL530xFirstTry/Src/gpio.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -mfloat-abi=soft '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F030x8 -I"D:/STM32projects/VL530xFirstTry/Inc" -I"D:/STM32projects/VL530xFirstTry/Drivers/STM32F0xx_HAL_Driver/Inc" -I"D:/STM32projects/VL530xFirstTry/Drivers/STM32F0xx_HAL_Driver/Inc/Legacy" -I"D:/STM32projects/VL530xFirstTry/Drivers/CMSIS/Device/ST/STM32F0xx/Include" -I"D:/STM32projects/VL530xFirstTry/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/User/i2c.o: D:/STM32projects/VL530xFirstTry/Src/i2c.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -mfloat-abi=soft '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F030x8 -I"D:/STM32projects/VL530xFirstTry/Inc" -I"D:/STM32projects/VL530xFirstTry/Drivers/STM32F0xx_HAL_Driver/Inc" -I"D:/STM32projects/VL530xFirstTry/Drivers/STM32F0xx_HAL_Driver/Inc/Legacy" -I"D:/STM32projects/VL530xFirstTry/Drivers/CMSIS/Device/ST/STM32F0xx/Include" -I"D:/STM32projects/VL530xFirstTry/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/User/main.o: D:/STM32projects/VL530xFirstTry/Src/main.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -mfloat-abi=soft '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F030x8 -I"D:/STM32projects/VL530xFirstTry/Inc" -I"D:/STM32projects/VL530xFirstTry/Drivers/STM32F0xx_HAL_Driver/Inc" -I"D:/STM32projects/VL530xFirstTry/Drivers/STM32F0xx_HAL_Driver/Inc/Legacy" -I"D:/STM32projects/VL530xFirstTry/Drivers/CMSIS/Device/ST/STM32F0xx/Include" -I"D:/STM32projects/VL530xFirstTry/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/User/stm32f0xx_hal_msp.o: D:/STM32projects/VL530xFirstTry/Src/stm32f0xx_hal_msp.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -mfloat-abi=soft '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F030x8 -I"D:/STM32projects/VL530xFirstTry/Inc" -I"D:/STM32projects/VL530xFirstTry/Drivers/STM32F0xx_HAL_Driver/Inc" -I"D:/STM32projects/VL530xFirstTry/Drivers/STM32F0xx_HAL_Driver/Inc/Legacy" -I"D:/STM32projects/VL530xFirstTry/Drivers/CMSIS/Device/ST/STM32F0xx/Include" -I"D:/STM32projects/VL530xFirstTry/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/User/stm32f0xx_it.o: D:/STM32projects/VL530xFirstTry/Src/stm32f0xx_it.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -mfloat-abi=soft '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F030x8 -I"D:/STM32projects/VL530xFirstTry/Inc" -I"D:/STM32projects/VL530xFirstTry/Drivers/STM32F0xx_HAL_Driver/Inc" -I"D:/STM32projects/VL530xFirstTry/Drivers/STM32F0xx_HAL_Driver/Inc/Legacy" -I"D:/STM32projects/VL530xFirstTry/Drivers/CMSIS/Device/ST/STM32F0xx/Include" -I"D:/STM32projects/VL530xFirstTry/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


