EESchema Schematic File Version 4
LIBS:jazzgoth-encoders-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x04 J1
U 1 1 5CACFED4
P 3600 4550
F 0 "J1" V 3564 4262 50  0000 R CNN
F 1 "Conn_01x04" V 3473 4262 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 3600 4550 50  0001 C CNN
F 3 "~" H 3600 4550 50  0001 C CNN
	1    3600 4550
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5CAD215A
P 3800 4550
F 0 "#PWR01" H 3800 4300 50  0001 C CNN
F 1 "GND" H 3805 4377 50  0000 C CNN
F 2 "" H 3800 4550 50  0001 C CNN
F 3 "" H 3800 4550 50  0001 C CNN
	1    3800 4550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3750 4450 3800 4450
Wire Wire Line
	4300 4450 4300 4350
Wire Wire Line
	4300 4350 5000 4350
Connection ~ 3800 4450
NoConn ~ 5000 4550
NoConn ~ 6600 3950
NoConn ~ 5700 3650
$Comp
L Device:C C9
U 1 1 5CAEE883
P 7650 4300
F 0 "C9" H 7765 4346 50  0000 L CNN
F 1 "100nF" H 7765 4255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7688 4150 50  0001 C CNN
F 3 "~" H 7650 4300 50  0001 C CNN
	1    7650 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 4150 7650 4150
$Comp
L power:GND #PWR07
U 1 1 5CAF0948
P 7650 4850
F 0 "#PWR07" H 7650 4600 50  0001 C CNN
F 1 "GND" H 7655 4677 50  0000 C CNN
F 2 "" H 7650 4850 50  0001 C CNN
F 3 "" H 7650 4850 50  0001 C CNN
	1    7650 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7650 4450 7650 4850
Wire Wire Line
	6600 4250 7450 4250
Wire Wire Line
	7450 4250 7450 4450
Wire Wire Line
	7450 4450 7650 4450
Connection ~ 7650 4450
$Comp
L Device:R R3
U 1 1 5CAF689A
P 4500 4800
F 0 "R3" H 4570 4846 50  0000 L CNN
F 1 "51k" H 4570 4755 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4430 4800 50  0001 C CNN
F 3 "~" H 4500 4800 50  0001 C CNN
	1    4500 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 4650 4150 4650
Wire Wire Line
	4150 4650 4500 4650
Connection ~ 4150 4650
Wire Wire Line
	4500 4950 4750 4950
Wire Wire Line
	4750 4950 4750 4650
Wire Wire Line
	4750 4650 5000 4650
$Comp
L power:GND #PWR05
U 1 1 5CAF938F
P 4500 4950
F 0 "#PWR05" H 4500 4700 50  0001 C CNN
F 1 "GND" H 4505 4777 50  0000 C CNN
F 2 "" H 4500 4950 50  0001 C CNN
F 3 "" H 4500 4950 50  0001 C CNN
	1    4500 4950
	1    0    0    -1  
$EndComp
Connection ~ 4500 4950
$Comp
L Device:CP1 C3
U 1 1 5CAFA9B9
P 4350 3550
F 0 "C3" H 4465 3596 50  0000 L CNN
F 1 "10uF" H 4465 3505 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-18_Kemet-A" H 4350 3550 50  0001 C CNN
F 3 "~" H 4350 3550 50  0001 C CNN
	1    4350 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 3400 4700 3400
$Comp
L power:GND #PWR04
U 1 1 5CAFCBB7
P 4500 3700
F 0 "#PWR04" H 4500 3450 50  0001 C CNN
F 1 "GND" H 4505 3527 50  0000 C CNN
F 2 "" H 4500 3700 50  0001 C CNN
F 3 "" H 4500 3700 50  0001 C CNN
	1    4500 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 3700 4500 3700
Wire Wire Line
	4500 3700 4700 3700
Connection ~ 4500 3700
$Comp
L Device:C C6
U 1 1 5CAFB6E7
P 4700 3550
F 0 "C6" H 4815 3596 50  0000 L CNN
F 1 "100nF" H 4815 3505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4738 3400 50  0001 C CNN
F 3 "~" H 4700 3550 50  0001 C CNN
	1    4700 3550
	1    0    0    -1  
$EndComp
Text GLabel 6600 4450 2    50   Input ~ 0
Vext
$Comp
L Device:C C5
U 1 1 5CB04BE7
P 4700 2650
F 0 "C5" H 4815 2696 50  0000 L CNN
F 1 "100nF" H 4815 2605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4738 2500 50  0001 C CNN
F 3 "~" H 4700 2650 50  0001 C CNN
	1    4700 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C2
U 1 1 5CB054F6
P 4300 2650
F 0 "C2" H 4415 2696 50  0000 L CNN
F 1 "10uF" H 4415 2605 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-18_Kemet-A" H 4300 2650 50  0001 C CNN
F 3 "~" H 4300 2650 50  0001 C CNN
	1    4300 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 2800 4500 2800
$Comp
L power:GND #PWR03
U 1 1 5CB0662E
P 4500 2800
F 0 "#PWR03" H 4500 2550 50  0001 C CNN
F 1 "GND" H 4505 2627 50  0000 C CNN
F 2 "" H 4500 2800 50  0001 C CNN
F 3 "" H 4500 2800 50  0001 C CNN
	1    4500 2800
	1    0    0    -1  
$EndComp
Connection ~ 4500 2800
Wire Wire Line
	4500 2800 4700 2800
Text GLabel 6600 4350 2    50   Input ~ 0
Vdda
$Comp
L Device:C C4
U 1 1 5CB08228
P 4700 1750
F 0 "C4" H 4815 1796 50  0000 L CNN
F 1 "100nF" H 4815 1705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4738 1600 50  0001 C CNN
F 3 "~" H 4700 1750 50  0001 C CNN
	1    4700 1750
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C1
U 1 1 5CB08BBA
P 4250 1750
F 0 "C1" H 4365 1796 50  0000 L CNN
F 1 "10uF" H 4365 1705 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-18_Kemet-A" H 4250 1750 50  0001 C CNN
F 3 "~" H 4250 1750 50  0001 C CNN
	1    4250 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 1900 4450 1900
Wire Wire Line
	4250 1600 4700 1600
$Comp
L power:GND #PWR02
U 1 1 5CB0A391
P 4450 1900
F 0 "#PWR02" H 4450 1650 50  0001 C CNN
F 1 "GND" H 4455 1727 50  0000 C CNN
F 2 "" H 4450 1900 50  0001 C CNN
F 3 "" H 4450 1900 50  0001 C CNN
	1    4450 1900
	1    0    0    -1  
$EndComp
Connection ~ 4450 1900
Wire Wire Line
	4450 1900 4700 1900
Text GLabel 6600 4550 2    50   Input ~ 0
Vddd
$Comp
L Device:C C7
U 1 1 5CB0AE41
P 7050 4650
F 0 "C7" H 7165 4696 50  0000 L CNN
F 1 "10nF" H 7165 4605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7088 4500 50  0001 C CNN
F 3 "~" H 7050 4650 50  0001 C CNN
	1    7050 4650
	0    1    1    0   
$EndComp
Wire Wire Line
	6600 4650 6900 4650
$Comp
L Device:C C8
U 1 1 5CB0C94E
P 7050 4950
F 0 "C8" H 7165 4996 50  0000 L CNN
F 1 "10nF" H 7165 4905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7088 4800 50  0001 C CNN
F 3 "~" H 7050 4950 50  0001 C CNN
	1    7050 4950
	0    1    1    0   
$EndComp
Wire Wire Line
	7200 4650 7200 4950
$Comp
L power:GND #PWR06
U 1 1 5CB0DF8C
P 7200 4950
F 0 "#PWR06" H 7200 4700 50  0001 C CNN
F 1 "GND" H 7205 4777 50  0000 C CNN
F 2 "" H 7200 4950 50  0001 C CNN
F 3 "" H 7200 4950 50  0001 C CNN
	1    7200 4950
	1    0    0    -1  
$EndComp
Connection ~ 7200 4950
NoConn ~ 6600 5150
NoConn ~ 6600 5350
NoConn ~ 6600 4050
NoConn ~ 5800 3650
NoConn ~ 5000 4850
NoConn ~ 5000 4950
NoConn ~ 5000 5050
NoConn ~ 5000 5150
NoConn ~ 5000 5250
NoConn ~ 5000 5350
NoConn ~ 5000 4250
NoConn ~ 5000 4150
NoConn ~ 5000 4050
NoConn ~ 5000 3950
Wire Wire Line
	3800 4450 4300 4450
Wire Wire Line
	6600 4950 6900 4950
Wire Wire Line
	6550 4950 6600 4950
Connection ~ 6600 4950
$Comp
L AM4096:AM4096 U1
U 1 1 5CACC279
P 5800 4650
F 0 "U1" H 5800 5739 50  0000 C CNN
F 1 "AM4096" H 5800 5830 50  0000 C CNN
F 2 "Package_SO:SSOP-28_3.9x9.9mm_P0.635mm" H 5800 4650 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/30325b.pdf" H 5800 4650 50  0001 C CNN
	1    5800 4650
	-1   0    0    1   
$EndComp
Wire Wire Line
	5000 4450 4500 4450
Wire Wire Line
	4500 4450 4500 4250
Wire Wire Line
	4500 4250 3800 4250
Wire Wire Line
	3800 4250 3800 4350
Wire Wire Line
	4700 2500 5200 2500
Connection ~ 4700 2500
Text GLabel 5400 2500 2    50   Input ~ 0
Vext
$Comp
L Device:CircuitBreaker_1P CB1
U 1 1 5CB26333
P 5200 2200
F 0 "CB1" H 5252 2246 50  0000 L CNN
F 1 "CircuitBreaker_1P" H 5252 2155 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x02_P1.00mm_Vertical" H 5200 2200 50  0001 C CNN
F 3 "~" H 5200 2200 50  0001 C CNN
	1    5200 2200
	1    0    0    -1  
$EndComp
Connection ~ 5200 2500
Wire Wire Line
	5200 2500 5400 2500
$Comp
L Device:CircuitBreaker_1P CB2
U 1 1 5CB278FA
P 5200 2800
F 0 "CB2" H 5252 2846 50  0000 L CNN
F 1 "CircuitBreaker_1P" H 5252 2755 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x02_P1.00mm_Vertical" H 5200 2800 50  0001 C CNN
F 3 "~" H 5200 2800 50  0001 C CNN
	1    5200 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 2500 4300 2500
Connection ~ 4300 2500
Wire Wire Line
	4300 2500 4700 2500
Wire Wire Line
	4150 2500 4150 4650
Wire Wire Line
	5200 3100 5200 3400
Wire Wire Line
	5200 3400 4700 3400
Connection ~ 4700 3400
Wire Wire Line
	5200 1900 5200 1600
Wire Wire Line
	5200 1600 4700 1600
Connection ~ 4700 1600
Text GLabel 5200 3400 2    50   Input ~ 0
Vdda
Text GLabel 5200 1600 2    50   Input ~ 0
Vddd
$EndSCHEMATC
