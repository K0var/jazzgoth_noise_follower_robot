EESchema Schematic File Version 4
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 10650 5800 2    50   Input ~ 0
ext_MIC6
Text GLabel 8750 5900 2    50   Input ~ 0
ext_MIC5
Text GLabel 10800 3900 2    50   Input ~ 0
ext_MIC4
Text GLabel 8950 4000 2    50   Input ~ 0
ext_MIC3
Text GLabel 11000 2150 2    50   Input ~ 0
ext_MIC2
Text GLabel 8950 2200 2    50   Input ~ 0
ext_MIC1
Wire Wire Line
	10400 1550 10400 2050
Text GLabel 10700 1550 2    50   Input ~ 0
ext_3v3
Connection ~ 10400 1550
$Comp
L Device:R R35
U 1 1 5CAE0FEF
P 10550 1550
F 0 "R35" V 10343 1550 50  0000 C CNN
F 1 "47k" V 10434 1550 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 10480 1550 50  0001 C CNN
F 3 "~" H 10550 1550 50  0001 C CNN
	1    10550 1550
	0    1    1    0   
$EndComp
$Comp
L Device:C C6
U 1 1 5CAE0FC7
P 9950 2250
F 0 "C6" V 10202 2250 50  0000 C CNN
F 1 "1uF" V 10111 2250 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9988 2100 50  0001 C CNN
F 3 "~" H 9950 2250 50  0001 C CNN
	1    9950 2250
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R31
U 1 1 5CAE0FBD
P 10250 2250
F 0 "R31" V 10457 2250 50  0000 C CNN
F 1 "1k" V 10366 2250 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 10180 2250 50  0001 C CNN
F 3 "~" H 10250 2250 50  0001 C CNN
	1    10250 2250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	11000 2600 11000 2150
Wire Wire Line
	10850 2600 11000 2600
Wire Wire Line
	10400 2600 10550 2600
Connection ~ 10400 2250
Wire Wire Line
	10400 2250 10400 2600
Wire Wire Line
	10450 2250 10400 2250
$Comp
L Device:R R36
U 1 1 5CAE0FAD
P 10700 2600
F 0 "R36" V 10493 2600 50  0000 C CNN
F 1 "100k" V 10584 2600 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 10630 2600 50  0001 C CNN
F 3 "~" H 10700 2600 50  0001 C CNN
	1    10700 2600
	0    1    1    0   
$EndComp
$Comp
L Device:R R30
U 1 1 5CAE0FA3
P 10250 1550
F 0 "R30" V 10043 1550 50  0000 C CNN
F 1 "47k" V 10134 1550 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 10180 1550 50  0001 C CNN
F 3 "~" H 10250 1550 50  0001 C CNN
	1    10250 1550
	0    1    1    0   
$EndComp
Text GLabel 8650 1600 2    50   Input ~ 0
ext_3v3
Connection ~ 8350 1600
Wire Wire Line
	8350 2100 8350 1600
$Comp
L Device:R R15
U 1 1 5CAE0F8C
P 8500 1600
F 0 "R15" V 8293 1600 50  0000 C CNN
F 1 "47k" V 8384 1600 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8430 1600 50  0001 C CNN
F 3 "~" H 8500 1600 50  0001 C CNN
	1    8500 1600
	0    1    1    0   
$EndComp
$Comp
L Device:C C2
U 1 1 5CAE0F64
P 7900 2300
F 0 "C2" V 8152 2300 50  0000 C CNN
F 1 "1uF" V 8061 2300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7938 2150 50  0001 C CNN
F 3 "~" H 7900 2300 50  0001 C CNN
	1    7900 2300
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R10
U 1 1 5CAE0F5A
P 8200 2300
F 0 "R10" V 8407 2300 50  0000 C CNN
F 1 "1k" V 8316 2300 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8130 2300 50  0001 C CNN
F 3 "~" H 8200 2300 50  0001 C CNN
	1    8200 2300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8950 2650 8950 2200
Wire Wire Line
	8800 2650 8950 2650
Wire Wire Line
	8350 2650 8500 2650
Connection ~ 8350 2300
Wire Wire Line
	8350 2300 8350 2650
Wire Wire Line
	8400 2300 8350 2300
Text GLabel 9600 1200 1    50   Input ~ 0
ext_3v3
$Comp
L Amplifier_Operational:LM358 U2
U 3 1 5CAE0F3F
P 9300 1300
F 0 "U2" V 8975 1300 50  0000 C CNN
F 1 "LM358" V 9066 1300 50  0000 C CNN
F 2 "Package_SO:SOIC-8-1EP_3.9x4.9mm_P1.27mm_EP2.29x3mm_ThermalVias" H 9300 1300 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2904-n.pdf" H 9300 1300 50  0001 C CNN
	3    9300 1300
	0    1    1    0   
$EndComp
$Comp
L Amplifier_Operational:LM358 U3
U 2 1 5CAE0F35
P 10700 2150
F 0 "U3" H 10700 2517 50  0000 C CNN
F 1 "LM358" H 10700 2426 50  0000 C CNN
F 2 "Package_SO:SOIC-8-1EP_3.9x4.9mm_P1.27mm_EP2.29x3mm_ThermalVias" H 10700 2150 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2904-n.pdf" H 10700 2150 50  0001 C CNN
	2    10700 2150
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:LM358 U2
U 1 1 5CAE0F2B
P 8650 2200
F 0 "U2" H 8650 2567 50  0000 C CNN
F 1 "LM358" H 8650 2476 50  0000 C CNN
F 2 "Package_SO:SOIC-8-1EP_3.9x4.9mm_P1.27mm_EP2.29x3mm_ThermalVias" H 8650 2200 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2904-n.pdf" H 8650 2200 50  0001 C CNN
	1    8650 2200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R17
U 1 1 5CAE0F21
P 8650 2650
F 0 "R17" V 8443 2650 50  0000 C CNN
F 1 "100k" V 8534 2650 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8580 2650 50  0001 C CNN
F 3 "~" H 8650 2650 50  0001 C CNN
	1    8650 2650
	0    1    1    0   
$EndComp
$Comp
L Device:R R9
U 1 1 5CAE0F17
P 8200 1600
F 0 "R9" V 7993 1600 50  0000 C CNN
F 1 "47k" V 8084 1600 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8130 1600 50  0001 C CNN
F 3 "~" H 8200 1600 50  0001 C CNN
	1    8200 1600
	0    1    1    0   
$EndComp
Wire Wire Line
	10050 5200 10050 5700
Text GLabel 10350 5200 2    50   Input ~ 0
ext_3v3
Connection ~ 10050 5200
$Comp
L Device:R R29
U 1 1 5CAB7858
P 10200 5200
F 0 "R29" V 9993 5200 50  0000 C CNN
F 1 "47k" V 10084 5200 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 10130 5200 50  0001 C CNN
F 3 "~" H 10200 5200 50  0001 C CNN
	1    10200 5200
	0    1    1    0   
$EndComp
$Comp
L Device:C C4
U 1 1 5CAB7830
P 9600 5900
F 0 "C4" V 9852 5900 50  0000 C CNN
F 1 "1uF" V 9761 5900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9638 5750 50  0001 C CNN
F 3 "~" H 9600 5900 50  0001 C CNN
	1    9600 5900
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R26
U 1 1 5CAB7826
P 9900 5900
F 0 "R26" V 10107 5900 50  0000 C CNN
F 1 "1k" V 10016 5900 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 9830 5900 50  0001 C CNN
F 3 "~" H 9900 5900 50  0001 C CNN
	1    9900 5900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10650 6250 10650 5800
Wire Wire Line
	10500 6250 10650 6250
Wire Wire Line
	10050 6250 10200 6250
Connection ~ 10050 5900
Wire Wire Line
	10050 5900 10050 6250
Wire Wire Line
	10100 5900 10050 5900
$Comp
L Device:R R33
U 1 1 5CAB7816
P 10350 6250
F 0 "R33" V 10143 6250 50  0000 C CNN
F 1 "100k" V 10234 6250 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 10280 6250 50  0001 C CNN
F 3 "~" H 10350 6250 50  0001 C CNN
	1    10350 6250
	0    1    1    0   
$EndComp
$Comp
L Device:R R25
U 1 1 5CAB780C
P 9900 5200
F 0 "R25" V 9693 5200 50  0000 C CNN
F 1 "47k" V 9784 5200 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 9830 5200 50  0001 C CNN
F 3 "~" H 9900 5200 50  0001 C CNN
	1    9900 5200
	0    1    1    0   
$EndComp
Text GLabel 8450 5300 2    50   Input ~ 0
ext_3v3
Connection ~ 8150 5300
Wire Wire Line
	8150 5800 8150 5300
$Comp
L Device:R R13
U 1 1 5CAB77F5
P 8300 5300
F 0 "R13" V 8093 5300 50  0000 C CNN
F 1 "47k" V 8184 5300 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8230 5300 50  0001 C CNN
F 3 "~" H 8300 5300 50  0001 C CNN
	1    8300 5300
	0    1    1    0   
$EndComp
$Comp
L Device:C C1
U 1 1 5CAB77CD
P 7700 6000
F 0 "C1" V 7952 6000 50  0000 C CNN
F 1 "1uF" V 7861 6000 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7738 5850 50  0001 C CNN
F 3 "~" H 7700 6000 50  0001 C CNN
	1    7700 6000
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R8
U 1 1 5CAB77C3
P 8000 6000
F 0 "R8" V 8207 6000 50  0000 C CNN
F 1 "1k" V 8116 6000 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 7930 6000 50  0001 C CNN
F 3 "~" H 8000 6000 50  0001 C CNN
	1    8000 6000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8750 6350 8750 5900
Wire Wire Line
	8600 6350 8750 6350
Wire Wire Line
	8150 6350 8300 6350
Connection ~ 8150 6000
Wire Wire Line
	8150 6000 8150 6350
Wire Wire Line
	8200 6000 8150 6000
$Comp
L Amplifier_Operational:LM358 U1
U 3 1 5CAB77A8
P 8950 5050
F 0 "U1" V 8625 5050 50  0000 C CNN
F 1 "LM358" V 8716 5050 50  0000 C CNN
F 2 "Package_SO:SOIC-8-1EP_3.9x4.9mm_P1.27mm_EP2.29x3mm_ThermalVias" H 8950 5050 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2904-n.pdf" H 8950 5050 50  0001 C CNN
	3    8950 5050
	0    1    1    0   
$EndComp
$Comp
L Amplifier_Operational:LM358 U1
U 2 1 5CAB779E
P 10350 5800
F 0 "U1" H 10350 6167 50  0000 C CNN
F 1 "LM358" H 10350 6076 50  0000 C CNN
F 2 "Package_SO:SOIC-8-1EP_3.9x4.9mm_P1.27mm_EP2.29x3mm_ThermalVias" H 10350 5800 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2904-n.pdf" H 10350 5800 50  0001 C CNN
	2    10350 5800
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:LM358 U1
U 1 1 5CAB7794
P 8450 5900
F 0 "U1" H 8450 6267 50  0000 C CNN
F 1 "LM358" H 8450 6176 50  0000 C CNN
F 2 "Package_SO:SOIC-8-1EP_3.9x4.9mm_P1.27mm_EP2.29x3mm_ThermalVias" H 8450 5900 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2904-n.pdf" H 8450 5900 50  0001 C CNN
	1    8450 5900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R14
U 1 1 5CAB778A
P 8450 6350
F 0 "R14" V 8243 6350 50  0000 C CNN
F 1 "100k" V 8334 6350 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8380 6350 50  0001 C CNN
F 3 "~" H 8450 6350 50  0001 C CNN
	1    8450 6350
	0    1    1    0   
$EndComp
$Comp
L Device:R R7
U 1 1 5CAB7780
P 8000 5300
F 0 "R7" V 7793 5300 50  0000 C CNN
F 1 "47k" V 7884 5300 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 7930 5300 50  0001 C CNN
F 3 "~" H 8000 5300 50  0001 C CNN
	1    8000 5300
	0    1    1    0   
$EndComp
Wire Wire Line
	10200 3300 10200 3800
Text GLabel 10500 3300 2    50   Input ~ 0
ext_3v3
Connection ~ 10200 3300
$Comp
L Device:R R32
U 1 1 5CA955B7
P 10350 3300
F 0 "R32" V 10143 3300 50  0000 C CNN
F 1 "47k" V 10234 3300 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 10280 3300 50  0001 C CNN
F 3 "~" H 10350 3300 50  0001 C CNN
	1    10350 3300
	0    1    1    0   
$EndComp
$Comp
L Device:C C5
U 1 1 5CA9558F
P 9750 4000
F 0 "C5" V 10002 4000 50  0000 C CNN
F 1 "1uF" V 9911 4000 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9788 3850 50  0001 C CNN
F 3 "~" H 9750 4000 50  0001 C CNN
	1    9750 4000
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R28
U 1 1 5CA95585
P 10050 4000
F 0 "R28" V 10257 4000 50  0000 C CNN
F 1 "1k" V 10166 4000 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 9980 4000 50  0001 C CNN
F 3 "~" H 10050 4000 50  0001 C CNN
	1    10050 4000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10800 4350 10800 3900
Wire Wire Line
	10650 4350 10800 4350
Wire Wire Line
	10200 4350 10350 4350
Connection ~ 10200 4000
Wire Wire Line
	10200 4000 10200 4350
Wire Wire Line
	10250 4000 10200 4000
$Comp
L Device:R R34
U 1 1 5CA9556B
P 10500 4350
F 0 "R34" V 10293 4350 50  0000 C CNN
F 1 "100k" V 10384 4350 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 10430 4350 50  0001 C CNN
F 3 "~" H 10500 4350 50  0001 C CNN
	1    10500 4350
	0    1    1    0   
$EndComp
$Comp
L Device:R R27
U 1 1 5CA95561
P 10050 3300
F 0 "R27" V 9843 3300 50  0000 C CNN
F 1 "47k" V 9934 3300 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 9980 3300 50  0001 C CNN
F 3 "~" H 10050 3300 50  0001 C CNN
	1    10050 3300
	0    1    1    0   
$EndComp
Text GLabel 8650 3400 2    50   Input ~ 0
ext_3v3
Connection ~ 8350 3400
Wire Wire Line
	8350 3900 8350 3400
$Comp
L Device:R R16
U 1 1 5CA891D4
P 8500 3400
F 0 "R16" V 8293 3400 50  0000 C CNN
F 1 "47k" V 8384 3400 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8430 3400 50  0001 C CNN
F 3 "~" H 8500 3400 50  0001 C CNN
	1    8500 3400
	0    1    1    0   
$EndComp
$Comp
L Device:C C3
U 1 1 5CA832AB
P 7900 4100
F 0 "C3" V 8152 4100 50  0000 C CNN
F 1 "1uF" V 8061 4100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7938 3950 50  0001 C CNN
F 3 "~" H 7900 4100 50  0001 C CNN
	1    7900 4100
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R12
U 1 1 5CA82B77
P 8200 4100
F 0 "R12" V 8407 4100 50  0000 C CNN
F 1 "1k" V 8316 4100 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8130 4100 50  0001 C CNN
F 3 "~" H 8200 4100 50  0001 C CNN
	1    8200 4100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8950 4450 8950 4000
Wire Wire Line
	8800 4450 8950 4450
Wire Wire Line
	8350 4450 8500 4450
Connection ~ 8350 4100
Wire Wire Line
	8350 4100 8350 4450
Wire Wire Line
	8400 4100 8350 4100
Text GLabel 10150 2900 1    50   Input ~ 0
ext_3v3
$Comp
L Amplifier_Operational:LM358 U3
U 3 1 5CA6409D
P 9850 3000
F 0 "U3" V 9525 3000 50  0000 C CNN
F 1 "LM358" V 9616 3000 50  0000 C CNN
F 2 "Package_SO:SOIC-8-1EP_3.9x4.9mm_P1.27mm_EP2.29x3mm_ThermalVias" H 9850 3000 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2904-n.pdf" H 9850 3000 50  0001 C CNN
	3    9850 3000
	0    1    1    0   
$EndComp
$Comp
L Amplifier_Operational:LM358 U2
U 2 1 5CA61460
P 10500 3900
F 0 "U2" H 10500 4267 50  0000 C CNN
F 1 "LM358" H 10500 4176 50  0000 C CNN
F 2 "Package_SO:SOIC-8-1EP_3.9x4.9mm_P1.27mm_EP2.29x3mm_ThermalVias" H 10500 3900 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2904-n.pdf" H 10500 3900 50  0001 C CNN
	2    10500 3900
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:LM358 U3
U 1 1 5CA5B728
P 8650 4000
F 0 "U3" H 8650 4367 50  0000 C CNN
F 1 "LM358" H 8650 4276 50  0000 C CNN
F 2 "Package_SO:SOIC-8-1EP_3.9x4.9mm_P1.27mm_EP2.29x3mm_ThermalVias" H 8650 4000 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2904-n.pdf" H 8650 4000 50  0001 C CNN
	1    8650 4000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R18
U 1 1 5CA56D5E
P 8650 4450
F 0 "R18" V 8443 4450 50  0000 C CNN
F 1 "100k" V 8534 4450 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8580 4450 50  0001 C CNN
F 3 "~" H 8650 4450 50  0001 C CNN
	1    8650 4450
	0    1    1    0   
$EndComp
$Comp
L Device:R R11
U 1 1 5CA56376
P 8200 3400
F 0 "R11" V 7993 3400 50  0000 C CNN
F 1 "47k" V 8084 3400 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8130 3400 50  0001 C CNN
F 3 "~" H 8200 3400 50  0001 C CNN
	1    8200 3400
	0    1    1    0   
$EndComp
Text Notes 9450 800  0    50   ~ 0
Microphone
$Comp
L Connector_Generic:Conn_01x02 J4
U 1 1 5CDB216E
P 7750 4300
F 0 "J4" V 7622 4380 50  0000 L CNN
F 1 "Conn_01x02" V 7713 4380 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 7750 4300 50  0001 C CNN
F 3 "~" H 7750 4300 50  0001 C CNN
	1    7750 4300
	0    1    1    0   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J6
U 1 1 5CDBCAA4
P 9600 4200
F 0 "J6" V 9472 4280 50  0000 L CNN
F 1 "Conn_01x02" V 9563 4280 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 9600 4200 50  0001 C CNN
F 3 "~" H 9600 4200 50  0001 C CNN
	1    9600 4200
	0    1    1    0   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J7
U 1 1 5CDC0824
P 9800 2450
F 0 "J7" V 9672 2530 50  0000 L CNN
F 1 "Conn_01x02" V 9763 2530 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 9800 2450 50  0001 C CNN
F 3 "~" H 9800 2450 50  0001 C CNN
	1    9800 2450
	0    1    1    0   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J3
U 1 1 5CDC1895
P 7750 2500
F 0 "J3" V 7622 2580 50  0000 L CNN
F 1 "Conn_01x02" V 7713 2580 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 7750 2500 50  0001 C CNN
F 3 "~" H 7750 2500 50  0001 C CNN
	1    7750 2500
	0    1    1    0   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J2
U 1 1 5CDC2A41
P 7550 6200
F 0 "J2" V 7422 6280 50  0000 L CNN
F 1 "Conn_01x02" V 7513 6280 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 7550 6200 50  0001 C CNN
F 3 "~" H 7550 6200 50  0001 C CNN
	1    7550 6200
	0    1    1    0   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J5
U 1 1 5CDC3E3D
P 9450 6100
F 0 "J5" V 9322 6180 50  0000 L CNN
F 1 "Conn_01x02" V 9150 6850 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 9450 6100 50  0001 C CNN
F 3 "~" H 9450 6100 50  0001 C CNN
	1    9450 6100
	0    1    1    0   
$EndComp
Connection ~ 7750 2300
$Comp
L Device:Q_NPN_CBE Q1
U 1 1 5CA65A9F
P 7200 5250
F 0 "Q1" V 7528 5250 50  0000 C CNN
F 1 "Q_NPN_CBE" V 7437 5250 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92" H 7400 5350 50  0001 C CNN
F 3 "~" H 7200 5250 50  0001 C CNN
	1    7200 5250
	0    -1   -1   0   
$EndComp
Text GLabel 7000 5150 0    50   Input ~ 0
ext_3v3
$Comp
L Device:R R1
U 1 1 5CA72D43
P 7000 5300
F 0 "R1" V 6793 5300 50  0000 C CNN
F 1 "47k" V 6884 5300 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 6930 5300 50  0001 C CNN
F 3 "~" H 7000 5300 50  0001 C CNN
	1    7000 5300
	-1   0    0    1   
$EndComp
$Comp
L Device:R R2
U 1 1 5CA73A85
P 7000 5600
F 0 "R2" V 6793 5600 50  0000 C CNN
F 1 "47k" V 6884 5600 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 6930 5600 50  0001 C CNN
F 3 "~" H 7000 5600 50  0001 C CNN
	1    7000 5600
	-1   0    0    1   
$EndComp
Wire Wire Line
	7200 5450 7000 5450
Connection ~ 7000 5450
Wire Wire Line
	7400 5150 7550 5150
Wire Wire Line
	7550 5150 7550 6000
Connection ~ 7550 6000
$Comp
L Device:Q_NPN_CBE Q3
U 1 1 5CA83CA5
P 7400 3350
F 0 "Q3" V 7728 3350 50  0000 C CNN
F 1 "Q_NPN_CBE" V 7637 3350 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92" H 7600 3450 50  0001 C CNN
F 3 "~" H 7400 3350 50  0001 C CNN
	1    7400 3350
	0    -1   -1   0   
$EndComp
Text GLabel 7200 3250 0    50   Input ~ 0
ext_3v3
$Comp
L Device:R R5
U 1 1 5CA83CAC
P 7200 3400
F 0 "R5" V 6993 3400 50  0000 C CNN
F 1 "47k" V 7084 3400 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 7130 3400 50  0001 C CNN
F 3 "~" H 7200 3400 50  0001 C CNN
	1    7200 3400
	-1   0    0    1   
$EndComp
$Comp
L Device:R R6
U 1 1 5CA83CB2
P 7200 3700
F 0 "R6" V 6993 3700 50  0000 C CNN
F 1 "47k" V 7084 3700 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 7130 3700 50  0001 C CNN
F 3 "~" H 7200 3700 50  0001 C CNN
	1    7200 3700
	-1   0    0    1   
$EndComp
Wire Wire Line
	7400 3550 7200 3550
Connection ~ 7200 3550
Wire Wire Line
	7600 3250 7750 3250
Wire Wire Line
	7750 3250 7750 4100
Connection ~ 7750 4100
$Comp
L Device:Q_NPN_CBE Q2
U 1 1 5CA899AB
P 7400 1550
F 0 "Q2" V 7728 1550 50  0000 C CNN
F 1 "Q_NPN_CBE" V 7637 1550 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92" H 7600 1650 50  0001 C CNN
F 3 "~" H 7400 1550 50  0001 C CNN
	1    7400 1550
	0    -1   -1   0   
$EndComp
Text GLabel 7200 1450 0    50   Input ~ 0
ext_3v3
$Comp
L Device:R R3
U 1 1 5CA899B2
P 7200 1600
F 0 "R3" V 6993 1600 50  0000 C CNN
F 1 "47k" V 7084 1600 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 7130 1600 50  0001 C CNN
F 3 "~" H 7200 1600 50  0001 C CNN
	1    7200 1600
	-1   0    0    1   
$EndComp
$Comp
L Device:R R4
U 1 1 5CA899B8
P 7200 1900
F 0 "R4" V 6993 1900 50  0000 C CNN
F 1 "47k" V 7084 1900 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 7130 1900 50  0001 C CNN
F 3 "~" H 7200 1900 50  0001 C CNN
	1    7200 1900
	-1   0    0    1   
$EndComp
Wire Wire Line
	7400 1750 7200 1750
Connection ~ 7200 1750
Wire Wire Line
	7600 1450 7750 1450
Wire Wire Line
	7750 1450 7750 2300
$Comp
L Device:Q_NPN_CBE Q6
U 1 1 5CA9850B
P 9450 1500
F 0 "Q6" V 9778 1500 50  0000 C CNN
F 1 "Q_NPN_CBE" V 9687 1500 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92" H 9650 1600 50  0001 C CNN
F 3 "~" H 9450 1500 50  0001 C CNN
	1    9450 1500
	0    -1   -1   0   
$EndComp
Text GLabel 9250 1400 0    50   Input ~ 0
ext_3v3
$Comp
L Device:R R23
U 1 1 5CA98512
P 9250 1550
F 0 "R23" V 9043 1550 50  0000 C CNN
F 1 "47k" V 9134 1550 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 9180 1550 50  0001 C CNN
F 3 "~" H 9250 1550 50  0001 C CNN
	1    9250 1550
	-1   0    0    1   
$EndComp
$Comp
L Device:R R24
U 1 1 5CA98518
P 9250 1850
F 0 "R24" V 9043 1850 50  0000 C CNN
F 1 "47k" V 9134 1850 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 9180 1850 50  0001 C CNN
F 3 "~" H 9250 1850 50  0001 C CNN
	1    9250 1850
	-1   0    0    1   
$EndComp
Wire Wire Line
	9450 1700 9250 1700
Connection ~ 9250 1700
Wire Wire Line
	9650 1400 9800 1400
Wire Wire Line
	9800 1400 9800 2250
Connection ~ 9800 2250
$Comp
L Device:Q_NPN_CBE Q5
U 1 1 5CAA4C30
P 9250 3250
F 0 "Q5" V 9578 3250 50  0000 C CNN
F 1 "Q_NPN_CBE" V 9487 3250 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92" H 9450 3350 50  0001 C CNN
F 3 "~" H 9250 3250 50  0001 C CNN
	1    9250 3250
	0    -1   -1   0   
$EndComp
Text GLabel 9050 3150 0    50   Input ~ 0
ext_3v3
$Comp
L Device:R R21
U 1 1 5CAA4C37
P 9050 3300
F 0 "R21" V 8843 3300 50  0000 C CNN
F 1 "47k" V 8934 3300 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8980 3300 50  0001 C CNN
F 3 "~" H 9050 3300 50  0001 C CNN
	1    9050 3300
	-1   0    0    1   
$EndComp
$Comp
L Device:R R22
U 1 1 5CAA4C3D
P 9050 3600
F 0 "R22" V 8843 3600 50  0000 C CNN
F 1 "47k" V 8934 3600 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8980 3600 50  0001 C CNN
F 3 "~" H 9050 3600 50  0001 C CNN
	1    9050 3600
	-1   0    0    1   
$EndComp
Wire Wire Line
	9250 3450 9050 3450
Connection ~ 9050 3450
Wire Wire Line
	9450 3150 9600 3150
Wire Wire Line
	9600 3150 9600 4000
Connection ~ 9600 4000
$Comp
L Device:Q_NPN_CBE Q4
U 1 1 5CAAAF67
P 9100 5150
F 0 "Q4" V 9428 5150 50  0000 C CNN
F 1 "Q_NPN_CBE" V 9337 5150 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92" H 9300 5250 50  0001 C CNN
F 3 "~" H 9100 5150 50  0001 C CNN
	1    9100 5150
	0    -1   -1   0   
$EndComp
Text GLabel 8900 5050 0    50   Input ~ 0
ext_3v3
$Comp
L Device:R R19
U 1 1 5CAAAF6E
P 8900 5200
F 0 "R19" V 8693 5200 50  0000 C CNN
F 1 "47k" V 8784 5200 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8830 5200 50  0001 C CNN
F 3 "~" H 8900 5200 50  0001 C CNN
	1    8900 5200
	-1   0    0    1   
$EndComp
$Comp
L Device:R R20
U 1 1 5CAAAF74
P 8900 5500
F 0 "R20" V 8693 5500 50  0000 C CNN
F 1 "47k" V 8784 5500 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8830 5500 50  0001 C CNN
F 3 "~" H 8900 5500 50  0001 C CNN
	1    8900 5500
	-1   0    0    1   
$EndComp
Wire Wire Line
	9100 5350 8900 5350
Connection ~ 8900 5350
Wire Wire Line
	9300 5050 9450 5050
Wire Wire Line
	9450 5050 9450 5900
Connection ~ 9450 5900
$Comp
L Connector_Generic:Conn_01x08 J1
U 1 1 5CB86683
P 6600 4500
F 0 "J1" H 6518 3875 50  0000 C CNN
F 1 "Conn_01x08" H 6518 3966 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x08_P2.54mm_Horizontal" H 6600 4500 50  0001 C CNN
F 3 "~" H 6600 4500 50  0001 C CNN
	1    6600 4500
	-1   0    0    1   
$EndComp
Text GLabel 6800 4800 2    50   Input ~ 0
ext_GND
Text GLabel 6800 4700 2    50   Input ~ 0
ext_3v3
Text GLabel 6800 4600 2    50   Input ~ 0
ext_MIC1
Text GLabel 6800 4500 2    50   Input ~ 0
ext_MIC2
Text GLabel 6800 4400 2    50   Input ~ 0
ext_MIC3
Text GLabel 6800 4300 2    50   Input ~ 0
ext_MIC4
Text GLabel 6800 4200 2    50   Input ~ 0
ext_MIC5
Text GLabel 6800 4100 2    50   Input ~ 0
ext_MIC6
Text GLabel 7450 6000 1    50   Input ~ 0
ext_GND
Text GLabel 7850 5300 3    50   Input ~ 0
ext_GND
Text GLabel 8900 5650 2    50   Input ~ 0
ext_GND
Text GLabel 9350 5900 1    50   Input ~ 0
ext_GND
Text GLabel 9750 5200 3    50   Input ~ 0
ext_GND
Text GLabel 8650 4950 0    50   Input ~ 0
ext_GND
Text GLabel 7200 3850 2    50   Input ~ 0
ext_GND
Text GLabel 7650 4100 1    50   Input ~ 0
ext_GND
Text GLabel 8050 3400 3    50   Input ~ 0
ext_GND
Text GLabel 9050 3750 2    50   Input ~ 0
ext_GND
Text GLabel 9500 4000 1    50   Input ~ 0
ext_GND
Text GLabel 9900 3300 3    50   Input ~ 0
ext_GND
Text GLabel 7650 2300 1    50   Input ~ 0
ext_GND
Text GLabel 7200 2050 3    50   Input ~ 0
ext_GND
Text GLabel 8050 1600 3    50   Input ~ 0
ext_GND
Text GLabel 9550 2900 1    50   Input ~ 0
ext_GND
Text GLabel 9250 2000 2    50   Input ~ 0
ext_GND
Text GLabel 9700 2250 1    50   Input ~ 0
ext_GND
Text GLabel 10100 1550 3    50   Input ~ 0
ext_GND
Text GLabel 9000 1200 0    50   Input ~ 0
ext_GND
Text GLabel 9250 4950 1    50   Input ~ 0
ext_3v3
Text GLabel 7000 5750 3    50   Input ~ 0
ext_GND
$EndSCHEMATC
